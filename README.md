# Le Démineur

Jeu du démineur codé avec VueJS, présentant 3 modes de difficulté : 
- Facile
- Intermédiaire
- Difficile

### Installation du projet

##### Avec Docker
```git clone git@gitlab.com:Ervin11/demineur.git```  
```cd demineur```  
```docker build -t demineur .```  
```docker run -it -p 8080:8080 --rm --name demineur demineur```

##### Avec Docker Compose
```git clone git@gitlab.com:Ervin11/demineur.git```  
```cd demineur```  
```docker-compose up```

##### Sans Docker
```git clone git@gitlab.com:Ervin11/demineur.git```  
```cd demineur```  
```npm install```  
```npm run serve```